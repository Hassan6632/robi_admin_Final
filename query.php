<?php include 'header.php';?>
<!--    [ Strat Section Area]-->
<section id="query" class="body-part">
    <div class="container">
        <div class="row">
            <div class="col-lg-6">
                <a href="new-query.php" class="single-query">
                    <i class="fa fa-file-code-o" aria-hidden="true"></i>
                    <span>New Query</span>
                </a>
            </div>
            <div class="col-lg-6">
                <a href="#" class="single-query">
                    <i class="fa fa-wrench" aria-hidden="true"></i>
                        <span>Update Query</span>

                </a>
            </div>
            <div class="col-lg-6">
                <a href="#" class="single-query">
                    <i class="fa fa-tachometer" aria-hidden="true"></i>
                        <span>Query Dashboard</span>

                </a>
            </div>

            <div class="col-lg-6">
                <a href="#" class="single-query">
                    <i class="fa fa-play" aria-hidden="true"></i>
                        <span>Query Menagenent Tutorial</span>
                </a>
            </div>
            <div class="col-lg-6">
                <a href="#" class="single-query">
                    <i class="fa fa-slideshare" aria-hidden="true"></i>
                        <span href="#">New Carousel</span>
                </a>
            </div>
            <div class="col-lg-6">
                <a href="#" class="single-query">
                    <i class="fa fa-sliders" aria-hidden="true"></i>
                        <span>Menagenent Carousel</span>
                </a>
            </div>
        </div>
    </div>
</section>
<!--    [Finish Section Area]-->
<?php include 'footer.php';?>
