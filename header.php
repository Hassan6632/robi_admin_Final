<!DOCTYPE html>
<html lang="en">

<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!--    [Site Title] -->
    <title>Robi Admin</title>


    <!--    [Bootstrap Css] -->
    <link rel="stylesheet" href="assets/css/bootstrap.min.css">

    <!--    [Animate Css]-->
    <link rel="stylesheet" href="assets/css/animate.css">

    <!--    [FontAwesome Css] -->
    <link rel="stylesheet" href="assets/css/font-awesome.min.css">

    <!--    [IcoFont Css] -->
    <link rel="stylesheet" href="assets/css/icofont.css">

    <!--    [OwlCarousel Css]-->
    <link rel="stylesheet" href="assets/css/owl.carousel.min.css">

    <!--    [Custom Stlesheet]-->
    <link rel="stylesheet" href="assets/css/nav.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/responsive.css">


    <!--    [Favicon] -->
    <link rel="shortcut icon" type="image/x-icon" href="assets/img/robi_logo_white.png">


</head>

<body>
    <!--PRELOADER START-->

    <!--    [ Strat Preloader Area]-->

    <div class="pre-loader-area">
        <div class="sk-folding-cube">
            <div class="sk-cube1 sk-cube"></div>
            <div class="sk-cube2 sk-cube"></div>
            <div class="sk-cube4 sk-cube"></div>
            <div class="sk-cube3 sk-cube"></div>
        </div>
    </div>


    <!--    [Finish Poreloader Area]-->
    <!--    [ Strat Header Area]-->
    <header>
        <!--    [ Strat MENU Area]-->
        <div class="header-top">
            <div class="container-fluid">
                <div class="row margin-0">
                    <div class="col-lg-3 padding-0">
                        <div class="brand-ico-left">
                            <a href="index.php"><img src="assets/img/robi_logo_white.png" alt=""> <p>Robi Dashboard</p></a>
                        </div>
                    </div>
                    <div class="col-lg-9 padding-0">
                        <div class="top-heade-bar text-right">
                            <div class="admin-name">
                                <ul class="top-bar-menu">
                                    <li><a href="#"><i class="fa fa-user-o" aria-hidden="true"></i> Admin Name</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="" class="side-menu-width">
            <div class="brand-ico-right">
                <i class="fa fa-bars" aria-hidden="true"></i>
            </div>
            <div class="nav-side-menu">
                <div class="brand-icon">
                    <div class="brand-ico-left">
                        <a href="index.php"><img src="assets/img/robi_logo_white.png" alt=""> <p>Robi Dashboard</p></a>
                    </div>
                </div>
                <div class="brand">
                    <a href="index.php">
                        <img src="assets/img/robi_logo_white.png" alt="">
                    </a>
                </div>

                <div class="menu-list">

                    <ul id="menu-content" class="menu-content">
                        <li>
                            <a href="index.php">
                          <i class="fa fa-dashboard fa-lg"></i> Dashboard
                          </a>
                        </li>

                        <li>
                            <a href="query.php"><i class="fa fa-gift fa-lg"></i> QUERY</a> <a href="#" data-toggle="collapse" data-target="#products" class="collapsed active"><span class="single-btn arrow"></span></a>
                        </li>
                        <ul class="sub-menu collapse" id="products">
                            <li class=""><a href="new-query.php">NEW QUERY</a></li>
                            <li><a href="menage-query.php">MENAGE QUERY</a></li>
                            <li><a href="query-dash.php">QUERY DASHBOARD</a></li>
                        </ul>


                        <li data-toggle="collapse" data-target="#service" class="collapsed">
                            <a href="#"><i class="fa fa-globe fa-lg"></i> CAMPAIGNS <span class="arrow"></span></a>
                        </li>
                        <ul class="sub-menu collapse" id="service">
                            <li><a href="daily-campaign.php">DAILY CAMPAIGNS</a></li>
                            <li><a href="weekly-campaigns.php">WEEKLY CAMPAIGNS</a></li>
                            <li><a href="monthly-campaign.php">MONTHLY CAMPAIGNS</a></li>
                        </ul>

                        <li>
                            <a href="adds.php">
                          <i class="fa fa-user fa-lg"></i> ADDS
                          </a>
                        </li>

                        <li data-toggle="collapse" data-target="#new" class="collapsed">
                            <a href="#"><i class="fa fa-cogs" aria-hidden="true"></i> SETTINGS <span class="arrow"></span></a>
                        </li>
                        <ul class="sub-menu collapse" id="new">
                            <li><a href="add-admin.php">ADD ADMIN</a></li>
                            <li><a href="add-account.php">ADD ACCOUNT</a></li>
                        </ul>


                        <li>
                            <a href="inbox.php">
                          <i class="fa fa-comments-o" aria-hidden="true"></i> INBOX/ MESSAGE
                          </a>
                        </li>

                        <li>
                            <a href="help-inquery.php">
                          <i class="fa fa-ambulance" aria-hidden="true"></i> HELP/INQUIRY
                          </a>
                        </li>
                    </ul>
                </div>
            </div>
            <!--    [Finish Logo Area]-->
        </div>
        <!--    [Finish MENU Area]-->
    </header>
    <!--    [Finish Header Area]-->
