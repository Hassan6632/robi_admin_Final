<?php include 'header.php';?>
<!--    [ Strat Section Area]-->
<section id="add-admin" class="body-part">
    <div class="container">
        <div class="log-table">
            <div class="log-table-cell">

                <div class="row justify-content-center">
                    <div class="col-lg-6">
                        <div class="log-form text-center">
                            <form action="">
                                <div class="input-group">
                                    <i class="fa fa-user-plus" aria-hidden="true"></i>
                                    <input type="name" class="form-control" placeholder="Name">
                                </div>
                                <div class="input-group">
                                    <i class="fa fa-envelope-o" aria-hidden="true"></i>
                                    <input type="email" class="form-control" placeholder="Email">
                                </div>
                                <div class="input-group">
                                    <i class="fa fa-key" aria-hidden="true"></i>
                                    <input type="password" class="form-control" placeholder="Password">
                                </div>
                                <div class="submit-btn">
                                    <button>Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!--    [Finish Section Area]-->
<?php include 'footer.php';?>
