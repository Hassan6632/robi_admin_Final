<?php include 'header.php';?>

<!--    [ Strat Section Title Area]-->
<section id="summery" class="body-part">
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-lg-6">
                <div class="page-heading text-center">
                    <h3>SUMMERY DASHBOARD</h3>
                </div>
            </div>
        </div>
        <div class="row">

            <div class="col-lg-3">
                <div class="dash-info glob border-color">
                    <div class="info-icon text-center">
                        <i class="icofont icofont-substitute"></i>
                    </div>
                    <div class="desh-info-txt">
                        <p>Total Subscriber</p>
                        <h4>2055</h4>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="dash-info face border-color-3">
                    <div class="info-icon info-ico-bg-3 text-center">
                        <i class="icofont icofont-substitute"></i>
                    </div>
                    <div class="desh-info-txt">
                        <p>Total Subscriber</p>
                        <h4>2055</h4>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="dash-info glob border-color-2">
                    <div class="info-icon info-ico-bg-2 text-center">
                        <i class="icofont icofont-ui-message"></i>
                    </div>
                    <div class="desh-info-txt">
                        <p>Total Message</p>
                        <h4>2055</h4>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="dash-info face border-color-4">
                    <div class="info-icon info-ico-bg-4 text-center">
                        <i class="icofont icofont-ui-message"></i>
                    </div>
                    <div class="desh-info-txt">
                        <p>Total Message</p>
                        <h4>2055</h4>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3">
                <div class="dash-info glob border-color">
                    <div class="info-icon text-center">
                        <i class="icofont icofont-substitute"></i>
                    </div>
                    <div class="desh-info-txt">
                        <p>Daily Subscriber</p>
                        <h4>2055</h4>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="dash-info face border-color-3">
                    <div class="info-icon info-ico-bg-3 text-center">
                        <i class="icofont icofont-substitute"></i>
                    </div>
                    <div class="desh-info-txt">
                        <p>Daily Message</p>
                        <h4>2055</h4>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="dash-info glob border-color-2">
                    <div class="info-icon info-ico-bg-2 text-center">
                        <i class="icofont icofont-ui-message"></i>
                    </div>
                    <div class="desh-info-txt">
                        <p>Today's User</p>
                        <h4>2055</h4>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="dash-info face border-color-4">
                    <div class="info-icon info-ico-bg-4 text-center">
                        <i class="icofont icofont-ui-message"></i>
                    </div>
                    <div class="desh-info-txt">
                        <p>Today's User</p>
                        <h4>2055</h4>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3">
                <div class="dash-info glob border-color">
                    <div class="info-icon text-center">
                        <i class="icofont icofont-substitute"></i>
                    </div>
                    <div class="desh-info-txt">
                        <p>Male</p>
                        <h4>2055</h4>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="dash-info face border-color-3">
                    <div class="info-icon info-ico-bg-3 text-center">
                        <i class="icofont icofont-substitute"></i>
                    </div>
                    <div class="desh-info-txt">
                        <p>Female</p>
                        <h4>2055</h4>
                    </div>
                </div>
            </div>
            <div class="col-lg-3">
                <div class="dash-info glob border-color-2">
                    <div class="info-icon info-ico-bg-2 text-center">
                        <i class="icofont icofont-ui-message"></i>
                    </div>
                    <div class="desh-info-txt">
                        <p>Denied</p>
                        <h4>2055</h4>
                    </div>
                </div>
            </div>

        </div>
    </div>

</section>
<!--    [Finish Section Title Area]-->




<?php include 'footer.php';?>
